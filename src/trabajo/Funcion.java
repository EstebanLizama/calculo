/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajo;

import com.bestcode.mathparser.IMathParser;
import com.bestcode.mathparser.MathParserFactory;

/**
 *
 * @author Esteban
 */
public class Funcion {

    String definicion;
    IMathParser parser = MathParserFactory.create();

    public Funcion(String def) {
        definicion = def;
        parser.setExpression(def);

    }

    public double eval(double x) throws Exception {
        double r = Double.NaN;
        parser.setX(x);
        r = parser.getValue();
        return r;
    }

    public double[] eval(double[] x) throws Exception {
        int n = x.length;
        double[] r = new double[n];
        for (int i = 0; i < n; i++) {
            r[i] = eval(x[i]);
        }
        return r;
    }

    public double[] rango(double x0, double xn, double d) {
        int n = (int) (Math.abs(xn - x0) / d);
        double[] r = new double[n];
        for (int i = 0; i < n; i++) {
            r[i] = x0;
            x0 += d;
        }
        return r;
    }

    public double longitudArco(double x[], double y[], int n) {
        double la = 0;
        int i = 0;
        while (i < (x.length - 1)) {
        la += Math.sqrt(Math.pow(x[i + 1] - x[i], 2) + Math.pow(y[i + 1] - y[i], 2));
        i = i + ((x.length - i) / n);
        i++;
        }

        return la;
    }
     public double longitudF(double a ,double b, double n) {
        double la = 0;
       
        double i = 1;
       while(i<=n){
        la=a+i*((b-a)/n);
        i++;
       }
        return la;
    }
    
    

}
